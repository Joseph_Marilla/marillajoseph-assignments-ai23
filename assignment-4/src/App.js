import React, {Component} from 'react';
import logo from './logo.svg';
import './App.css';

import GoogleLogin from 'react-google-login';

class App extends Component {

  render() {

    const responseGoogle = (response) => {
      console.log(response);
    }

    return (
      <div className="App">
        <h1>LOGIN GOOGLE</h1>

      <GoogleLogin
        clientId="493679794295-i8ifiu64rkt9rs03jp514hi1q96ecg69.apps.googleusercontent.com" //CLIENTID NOT CREATED YET
        buttonText="login"
        onSuccess={responseGoogle}
        onFailure={responseGoogle}
      />

      </div>
    );
  }
}

export default App;

